<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    /**
     * @var string
     */
    private $me;

    public function __construct(string $me)
    {
        $this->me = $me;
    }

    /**
     * @Route("/")
     */
    public function index() :Response
    {
        return new Response(
            '<h1>Hello World</h1>',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }

    /**
     * @Route("/me")
     */
    public function show() :Response
    {
        return new Response(sprintf(
            '<h1>Hello World</h1><p>My name is %s </p>',
            $this->me
        ),
            Response::HTTP_OK,
            ['content-type' => 'text/html']);
    }

}